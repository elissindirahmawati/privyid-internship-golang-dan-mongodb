module mongo-golang

go 1.19

require (
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	labix.org/v2/mgo v0.0.0-20140701140051-000000000287
)
